import http from "../http-common";

class AlbumService {
  getAll() {
    return http.get("/albums");
  }
}

export default new AlbumService();